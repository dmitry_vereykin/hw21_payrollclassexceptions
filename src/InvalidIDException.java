/**
 * Created by Dmitry on 7/13/2015.
 */
public class InvalidIDException extends Exception {
    public InvalidIDException(int id) {
        super("Invalid ID number: " + id);
    }
}
