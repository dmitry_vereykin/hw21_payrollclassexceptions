/**
 * Created by Dmitry on 7/13/2015.
 */
public class InvalidNameException extends Exception {
    public InvalidNameException() {
        super("Invalid name");
    }
}
